package io.swagger.stepdefs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class StatusCode extends AbstractRequestBuilder {

    @When("^I make a proper GET request$")
    public void whenIMakeAProperGetRequest() {
        makeGetRequest("/api/swagger.json");
    }

    @When("^I make an improper GET request$")
    public void whenIMakeAnImproperGetRequest() {
        makeGetRequest("/swagger.json");
    }

    @Then("^Status code 200 returned$")
    public void thenStatusCodeTwoHunderedReturned() {
        Assert.assertEquals(200, response.getStatusCode());
    }

    @Then("^Status code 404 returned$")
    public void thenStatusCodeFourHunderedAnFourIsReturned() {
        Assert.assertEquals(404, response.getStatusCode());
    }
}
