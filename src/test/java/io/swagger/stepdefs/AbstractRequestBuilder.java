package io.swagger.stepdefs;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class AbstractRequestBuilder {

    private final String baseUri = "http://generator.swagger.io";

    RequestSpecification request = RestAssured.given();
    Response response;

    public void makeGetRequest(String path) {
        request.baseUri(baseUri);
        response = request.get(path);
    }
}
