@statusCode
Feature: statusCode

  Scenario: Make a proper GET request to http://generator.swagger.io
    When I make a proper GET request
    Then Status code 200 returned

  Scenario: Make a improper GET request to http://generator.swagger.io/
    When I make an improper GET request
    Then Status code 404 returned